  $('#morePhotos').click(function() {
    var portfolioContainer = document.getElementById('portfolioContainer');
    var body = document.getElementById('page-top');
    childs=portfolioContainer.children;
    var hijos = childs.length+1;
    if (hijos<34){
    	if(hijos<30){
    		aumento=6;
    	}else{
    		aumento=4;
    	}
	    for(var i=hijos; i<hijos+aumento; i++){
	    	var row = document.createElement('div');
		    row.setAttribute('class','col-md-4 col-sm-6 portfolio-item');
		    portfolioContainer.appendChild(row);

		    var portfolioLink = document.createElement('a');
		    portfolioLink.setAttribute('class','portfolio-link');
		    portfolioLink.setAttribute('data-toggle','modal');
		    portfolioLink.setAttribute('href','#portfolioModal'+i);
		    row.appendChild(portfolioLink);

		    var portfolioHover = document.createElement('div');
		    portfolioHover.setAttribute('class','portfolio-hover');
		    portfolioLink.appendChild(portfolioHover);

		    var portfolioHoverContent = document.createElement('div');
		    portfolioHoverContent.setAttribute('class','portfolio-hover-content');
		    portfolioHover.appendChild(portfolioHoverContent);

		    var faPlus = document.createElement('i');
		    faPlus.setAttribute('class', 'fa fa-plus fa-3x');
		    portfolioHoverContent.appendChild(faPlus);

		    var portfolioImagen = document.createElement('img');
		    portfolioImagen.setAttribute('class','img-fluid');
		    if(i>9){
		    	portfolioImagen.setAttribute('src', 'img/portfolio/'+i+'-thumbnail.jpg');
		    }else{
		    	portfolioImagen.setAttribute('src', 'img/portfolio/0'+i+'-thumbnail.jpg');
		    }
		    portfolioLink.appendChild(portfolioImagen);

		    var portfolioCaption = document.createElement('div');
		    portfolioCaption.setAttribute('class','portfolio-caption');
		    row.appendChild(portfolioCaption);

		    var portfolioModal = document.createElement('div');
		    portfolioModal.setAttribute('class','portfolio-modal modal fade');
		    portfolioModal.setAttribute('id','portfolioModal'+i);
		    portfolioModal.setAttribute('tabindex','-1');
		    portfolioModal.setAttribute('role','dialog');
		    portfolioModal.setAttribute('aria-hidden','true');
		    body.appendChild(portfolioModal);

		    var modalDialog = document.createElement('div');
		    modalDialog.setAttribute('class','modal-dialog');
		    portfolioModal.appendChild(modalDialog);

		    var modalContent = document.createElement('div');
		    modalContent.setAttribute('class','modal-content');
		    modalDialog.appendChild(modalContent);

		    var closeModal = document.createElement('div');
		    closeModal.setAttribute('class','close-modal');
		    closeModal.setAttribute('data-dismiss','modal');
		    modalContent.appendChild(closeModal);

		    var lr = document.createElement('div');
		    lr.setAttribute('class','lr');
		    closeModal.appendChild(lr);

		    var rl = document.createElement('div');
		    rl.setAttribute('class','rl');
		    lr.appendChild(rl);

		    var containerModal = document.createElement('div');
		    containerModal.setAttribute('class','container');
		    modalContent.appendChild(containerModal);

		    var rowModal = document.createElement('div');
		    rowModal.setAttribute('class','row');
		    containerModal.appendChild(rowModal);

		    var colModal = document.createElement('div');
		    colModal.setAttribute('class','col-lg-8 mx-auto');
		    rowModal.appendChild(colModal);

		    var bodyModal = document.createElement('div');
		    bodyModal.setAttribute('class','modal-body');
		    colModal.appendChild(bodyModal);

		    var modalImagen = document.createElement('img');
		    modalImagen.setAttribute('class','img-fluid d-block mx-auto');
		    if(i>9){
		    	modalImagen.setAttribute('src', 'img/portfolio/'+i+'-full.jpg');
		    }else{
		    	modalImagen.setAttribute('src', 'img/portfolio/0'+i+'-full.jpg');
		    }
		    bodyModal.appendChild(modalImagen);

		    var buttonCloseModal = document.createElement('button');
		    buttonCloseModal.setAttribute('class','btn btn-primary');
		    buttonCloseModal.setAttribute('data-dismiss','modal');
		    buttonCloseModal.setAttribute('type','button');
		    bodyModal.appendChild(buttonCloseModal);

			var faTimes = document.createElement('i');
		    faTimes.setAttribute('class', 'fa fa-times');
		    buttonCloseModal.appendChild(faTimes);

		    var textButton = document.createTextNode(" Close Project");
		    buttonCloseModal.appendChild(textButton);
	    }
	}else{
		alert('No hay mas fotos');
	}

  });