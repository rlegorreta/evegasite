
var mapa = document.getElementById('map');
mapboxgl.accessToken = 'pk.eyJ1Ijoiam9zaWFzMjIiLCJhIjoiY2plZzg3eHloMmE2ZjJ2bG5rdXgwd3huZSJ9.t8a5LAvC4dYKkjou8TiU5Q';
var map = new mapboxgl.Map({
container: mapa,
style: 'mapbox://styles/mapbox/streets-v10',
zoom: 10,
center: [-118.4182337, 34.255602]
});

var geojson = {
  type: 'FeatureCollection',
  features: [{
    type: 'Feature',
    geometry: {
      type: 'Point',
      coordinates: [-118.4182337, 34.255602]
    },
    properties: {
      title: 'Mapbox',
      description: 'Washington, D.C.'
    }
  }]
};

// add markers to map
geojson.features.forEach(function(marker) {

  // create a HTML element for each feature
  var el = document.createElement('div');
  el.className = 'marker';

  // make a marker for each feature and add to the map
  new mapboxgl.Marker(el)
  .setLngLat(marker.geometry.coordinates)
  .addTo(map);
});

map.scrollZoom.disable();

anchoPantalla = window.innerWidth;

if(anchoPantalla<700){
  map['dragPan'].disable();
}